﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using PS3Lib;
using System.Diagnostics;

namespace eboot_dumper_cli
{
    class Program
    {
        public static CCAPI ps3 = new CCAPI();
        private static uint[] procs;
        public static WebClient webClient = new WebClient();

        public static bool DownloadEboot(string ip)
        {
            ps3.GetProcessList(out procs);
            bool status = false;
            for (int i = 0; i < procs.Length; i++)
            {
                string name = String.Empty;
                ps3.GetProcessName(procs[i], out name);
                if (name.EndsWith("EBOOT.BIN"))
                {
                    string urlToGoTo = "http://" + ip + name;
                    Console.WriteLine("EBOOT Location: " + name);
                    string pathToSave = Directory.GetCurrentDirectory() + "\\";
                    webClient.DownloadFile(urlToGoTo, @"" + pathToSave + "EBOOT.BIN");
                    Process.Start("explorer.exe", @"" + pathToSave);
                    status = true;
                    Console.WriteLine("The EBOOT.BIN has been saved to " + pathToSave);
                    Console.ReadKey();
                    break;

                }
                else
                {
                    Console.WriteLine("EBOOT Location: Not found");
                    Console.ReadKey();
                    status = false;
                }

            }
            return status;
        }
        static void Main(string[] args)
        {
            string consoleIP;
            Console.WriteLine("Welcome to eboot-dumper");
            Console.Write("Please enter your PS3 IP:");
            consoleIP = Console.ReadLine();
            if (ps3.SUCCESS(ps3.ConnectTarget(consoleIP)))
            {
                ps3.SUCCESS(ps3.AttachProcess());
                Console.WriteLine("Console connected and attached, press enter to dump the current EBOOT.");
                Console.ReadKey();
                DownloadEboot(consoleIP);

            }
            else
            {
                Console.WriteLine("Something went wrong, exiting...");
            }

        }
    }
}
